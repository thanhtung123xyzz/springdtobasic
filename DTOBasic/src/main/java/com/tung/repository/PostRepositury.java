package com.tung.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tung.entity.PostEntity;

@Repository
public interface PostRepositury extends JpaRepository<PostEntity, Long> {

}
