package com.tung.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tung.dto.PostDto;
import com.tung.entity.PostEntity;
import com.tung.service.IPostService;

@RestController
@RequestMapping("/api/posts")
public class PostController {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private IPostService iPostService;

	@GetMapping
	public List<PostDto> getAllPost() {
		return iPostService.getAllPosts().stream().map(post -> modelMapper.map(post, PostDto.class))
				.collect(Collectors.toList());
	}

	@GetMapping("/{id}")
	public ResponseEntity<PostDto> getPostById(@PathVariable(name = "id") Long id) {
		PostEntity post = iPostService.getPostById(id);

		// convert entity to dto
		PostDto postResponse = modelMapper.map(post, PostDto.class);

		return ResponseEntity.ok().body(postResponse);
	}
	
	@PostMapping
	public ResponseEntity<PostEntity> createPost(@RequestBody PostDto postDto){
		//convert dto to entity
		PostEntity postRequest = modelMapper.map(postDto, PostEntity.class);
		
		PostEntity post = iPostService.createPost(postRequest);
		
		//convert entity to dto
		PostDto dto = modelMapper.map(post, PostDto.class);
		
		return new ResponseEntity<PostEntity>(postRequest, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PostDto> updatePost(@PathVariable long id, @RequestBody PostDto postDto){
		//conver dto to entity
		PostEntity postRequest = modelMapper.map(postDto, PostEntity.class);
		
		PostEntity post = iPostService.updatePost(id, postRequest);
		
		//conver entity to dto
		PostDto dto = modelMapper.map(post, PostDto.class);
		
		return ResponseEntity<PostDto>.ok().body(postRequest);
	}
	

}
