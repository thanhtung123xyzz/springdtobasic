package com.tung.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tung.entity.PostEntity;
import com.tung.repository.PostRepositury;

@Service
public class PostServiceImpl implements IPostService {

	@Autowired
	PostRepositury postRepositury;

	@Override
	public List<PostEntity> getAllPosts() {
		return postRepositury.findAll();
	}

	@Override
	public PostEntity createPost(PostEntity post) {
		return postRepositury.save(post);
	}

	@Override
	public PostEntity updatePost(Long id, PostEntity post) {
		PostEntity postup = postRepositury.findById(id);
//				.orElseThrow(() -> new ResourceNotFoundException("PostEntity", "id", id));
		postup.setTitle(post.getTitle());
		post.setDescription(post.getDescription());
		postup.setContent(post.getContent());
		return postRepositury.save(postup);
	}

	@Override
	public void deletePost(Long id) {
		PostEntity post = postRepositury.findById(id);
//		.orElseThrow(() -> new ResourceNotFoundException("Post", "id", id));
		return postRepositury.delete(post);
	}

	@Override
	public PostEntity getPostById(Long id) {
		Optional<PostEntity> result = postRepositury.findById(id);
		
		if(result.isPresent()) {
			return result.get();
		}else {
			throw new ResourceNotFoundException("Post", "id", id);
		}
		return null;
	}
}
