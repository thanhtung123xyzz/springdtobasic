package com.tung.service;

import java.util.List;

import com.tung.entity.PostEntity;

public interface IPostService {
	List<PostEntity> getAllPosts();
	
	PostEntity createPost(PostEntity post);
	
	PostEntity updatePost(Long id,PostEntity post);
	
	void deletePost(Long id);
	
	PostEntity getPostById(Long id);
}
